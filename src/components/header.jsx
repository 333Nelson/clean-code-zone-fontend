export const Header = (props) => {
  return (
    <header id='header'>
      <div className='intro'>
        <div className='overlay'>
          <div className='container'>
            <div className='row'>
              <div className='col-md-8 col-md-offset-2 intro-text'>
                {/*<img src={props.data ? props.data.img : "loading..."} alt="Logo" />*/}
                <h1>
                  {props.data ? props.data.title : 'Loading'}
                </h1>
                <h2>
                  {props.data ? props.data.subtitle : 'Loading'}
                </h2>
                <p>{props.data ? props.data.paragraph : 'Loading'}</p>
                <a
                  href='#features'
                  className='btn btn-custom btn-lg page-scroll'
                >
                  CCZone Trainings
                </a>{' '}
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  )
}
